package com.dev.sampleofsql

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.dev.sandbox.MyDBManager
import com.dev.sandbox.R

class MainActivity : AppCompatActivity() {

    val myDBManager = MyDBManager(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)



        val btn = findViewById<Button>(R.id.btnSave)
        val title = findViewById<EditText>(R.id.etTitle)
        val content = findViewById<EditText>(R.id.etContent)
        val dataFromDB = findViewById<TextView>(R.id.tvDataFromDB)

        btn.setOnClickListener {
            dataFromDB.text = ""
            myDBManager.openDb()
            myDBManager.insertToDB(title.text.toString(), content.text.toString())
            val dataList = myDBManager.readDBData()
            for(item in dataList) {
                dataFromDB.append(item)
                dataFromDB.append("\n")
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        myDBManager.closeDB()
    }
}