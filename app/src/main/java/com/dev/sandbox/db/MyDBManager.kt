package com.dev.sandbox

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.dev.sandbox.db.MyDbNameClass

class MyDBManager(val context: Context) {

    val myDBHelper = MyDBHelper(context)
    var db: SQLiteDatabase? = null

    fun openDb() {
        db = myDBHelper.writableDatabase
    }

    fun insertToDB(title: String, content: String) {

        val values = ContentValues().apply {
            put(MyDbNameClass.COLUMN_NAME_TITLE, title)
            put(MyDbNameClass.COLUMN_NAME_CONTENT, content)
        }

        db?.insert(MyDbNameClass.TABLE_NAME, null, values)

    }

    fun readDBData(): ArrayList<String> {
        val dataList = ArrayList<String>()

        val cursor = db?.query(MyDbNameClass.TABLE_NAME, null,
                null, null, null, null, null)

        with(cursor) {
            while(this?.moveToNext()!!){
                val dataText = cursor?.getString(cursor.getColumnIndex(MyDbNameClass.COLUMN_NAME_TITLE))
                dataList.add(dataText.toString())
            }
        }
        cursor?.close()
        return dataList
    }

    fun closeDB() {
        myDBHelper.close()
    }
}